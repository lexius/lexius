<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'category' => 'World',
            ],
            [
                'category' => 'Business',
            ],
            [
                'category' => 'Tech',
            ],
            [
                'category' => 'Science',
            ],
            [
                'category' => 'Stories',
            ],
            [
                'category' => 'Entertainment & Arts',
            ],
            [
                'category' => 'Health',
            ],
            [
                'category' => 'Sport',
            ],
            [
                'category' => 'Travel',
            ],
            [
                'category' => 'Future',
            ],
        ];

        DB::table('categories')->insert($data);
    }
}

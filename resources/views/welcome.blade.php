@extends('layouts.app')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/css/welcome.css">
@endsection

@section('body')

    <div class="images-line ws">
        <div style="background-image:url(/images/head-pix/5.jpg)"></div>
        <div style="background-image:url(/images/head-pix/3.jpg)"></div>
        <div style="background-image:url(/images/head-pix/6.jpg)"></div>
    </div>

    <div class="banner ws" style="background-image:url(/images/head-pix/122.jpg)">
        {{ __('messages.success_formula_step_1') }}<br>
        {{ __('messages.success_formula_step_2') }}<br>
        {{ __('messages.success_formula_step_3') }}<br>
        <a href="{{ route('articles.index') }}" class="green-button">Start reading, it's free &rarr;</a>
    </div>
    <br><br>
    <h2 align="center">{{ __('messages.title_fresh_articles') }}:</h2><br>

    <div class="articles">
        @foreach ($articles['fresh'] as $article)
            @include('articles.includes.get_article')
        @endforeach
    </div>
    <br><br><br><br>
    <h2 align="center">{{ __('messages.article_search') }}</h2>
    <br>

    @include('includes.search')

    <br><br><br><br>

    @if(count($articles['beginners']))
        <h2 align="center">{{ __('messages.title_articles_for_beginners') }}:</h2><br>
        <div class="articles">
            @foreach ($articles['beginners'] as $article)
                @include('articles.includes.get_article')
            @endforeach
        </div>
        <br><br>
    @endif


    <div class="images-line">
        <div style="background-image:url(/images/head-pix/1.jpg)"></div>
        <div style="background-image:url(/images/head-pix/2.jpg)"></div>
        <div style="background-image:url(/images/head-pix/4.jpg)"></div>
    </div>

@endsection
@extends('layouts.app')

@section('body')
    <section>
        @foreach($words as $word)
            {{ $word['en'] }} | ==== | {{ $word['ru'] }}
            <br>
        @endforeach
        @if($words->total() > $words->count())
            {{ $words->links() }}
        @endif
    </section>
@endsection
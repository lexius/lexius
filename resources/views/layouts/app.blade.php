<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Lexius — improve your lexicon!">
    <title>Lexius — improve your lexicon!</title>
    <link rel="stylesheet" type="text/css" href="/css/layout.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/css/includes.css">
    <link rel="stylesheet" type="text/css" href="/css/article.css">
    <link rel="stylesheet" type="text/css" href="/libs/bootstrap-4/css/bootstrap.css">
    @yield('stylesheets')

</head>
<body>
<div class="body-container">
    <header>
        <div class="header-container">
            <a href="/" class="header-left">
                Lexius<br>
                improve your lexicon
            </a>
            <div class="menu">
                <a href="{{ route('articles.index') }}" data-menu="articles">{{ __('messages.menu_read_articles') }}</a>
                <a href="{{ route('words.list') }}" data-menu="words">{{ __('messages.menu_words_list') }}</a>
                {{--                <a href="{{ route('articles.create') }}" data-menu="articles">{{ __('messages.menu_add_article') }}</a>--}}
                @guest
                    <a href="{{ route('google.login') }}" data-menu="account">{{ __('messages.menu_login') }}</a>
                @else
                    <a href="{{ url('/logout') }}" data-menu="account"
                       style="background-image:url({{ Auth::user()->picture }})">{{ __('messages.menu_logout') }}</a>
                @endguest
            </div>
        </div>
    </header>
    <main>
        @yield('body')
    </main>
</div>

<footer>
    <div>

        <div>
            @for ($i = 0; $i < 5; $i++)
                @if(isset($categories[$i]))
                    <a href="">{{ $categories[$i]->category }}</a>
                @endif
            @endfor
        </div>
        <div>
            @for ($i = 5; $i < 10; $i++)
                @if(isset($categories[$i]))
                    <a href="">{{ $categories[$i]->category }}</a>
                @endif
            @endfor
        </div>
        <div>
            Menu:<br><br>
            <a href="{{ route('articles.index') }}" data-menu="articles">{{ __('messages.menu_read_articles') }}</a>
            <a href="{{ route('words.list') }}" data-menu="words">{{ __('messages.menu_words_list') }}</a>
            {{--            <a href="{{ route('articles.create') }}" data-menu="articles">{{ __('messages.menu_add_article') }}</a>--}}
        </div>
        <div>
            Account:<br><br>
            @guest
                <a href="{{ route('google.login') }}" data-menu="account">{{ __('messages.menu_login') }}</a>
            @else
                <a href="{{ url('/logout') }}" data-menu="account">{{ __('messages.menu_logout') }}</a>
            @endguest
        </div>
        <div>
            Mode:<br><br>
            @if (App::isLocale('en'))
                <a href="{{ route('user.set_locale', ['locale' => 'ru']) }}">rus &larr;&rarr; eng</a>
            @endif

            @if (App::isLocale('ru'))
                <a href="{{ route('user.set_locale', ['locale' => 'en']) }}">eng &larr;&rarr; spa</a>
            @endif
        </div>
    </div>
    <section>
        @2019 Lexius.com — Improve your lexicon.<br>
        Beta-version, made by couple of enthusiasts from <a href="http://a2-global.com">A2-Global</a>.
    </section>
</footer>

<script src="/libs/jquery/jquery-3.4.1.min.js"></script>
<script src="/js/article.js"></script>

</body>
</html>
<form action="{{ route('articles.index') }}" method="GET">
    <div class="search ws">
            <div>
                <label>{{ __('messages.article_category') }}:</label><br>
                <select name="category">
                    <option value="0">{{ __('messages.article_search_all_options') }}</option>
                    @foreach($articleCategories as $category)
                        <option value="{{ $category->id }}" {{ (isset($search['category']) && ($search['category'] == $category->id)) ? 'selected' : ''}}>{{ $category->category }}</option>
                    @endforeach
                </select><br><br>
                <label>{{ __('messages.article_keywords_in_title') }}:</label><br>
                <input type="text" name="keyword" value="{{ $search['keyword'] or '' }}"
                       placeholder="{{ __('messages.article_keywords_in_title_placeholder') }}">
            </div>
            <div>
                <label>{{ __('messages.article_level') }}:</label>
                <select name="level">
                    <option value="0">{{ __('messages.article_search_all_options') }}</option>
                    <option value="1" {{ (isset($search['level']) && ($search['level'] == 1)) ? 'selected' : ''}}>{{ __('messages.level_1') }}</option>
                    <option value="2" {{ (isset($search['level']) && ($search['level'] == 2)) ? 'selected' : ''}}>{{ __('messages.level_2') }}</option>
                    <option value="3" {{ (isset($search['level']) && ($search['level'] == 3)) ? 'selected' : ''}}>{{ __('messages.level_3') }}</option>
                </select><br><br>
                <input type="submit" class="green-button" value="{{ __('messages.article_search_submit') }} &rarr;">
            </div>
    </div>
</form>

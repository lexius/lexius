<div class="categories">
    @foreach($categories as $category)
        <a href="/articles/category/{{ $category->id }}">{{ $category->category }}</a>
    @endforeach
</div>
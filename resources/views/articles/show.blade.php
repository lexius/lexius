@extends('layouts.app')

@section('body')
    <div id="article-translate">
        <div>
            <span id="article-translate-original">Original: </span>
            <span id="article-translate-translated">Translated</span>
        </div>
        <div id="article-translate-sentence">Translated sentence</div>
        <div id="article-translate-preloader">{{ __('messages.article_translate_in_progress') }}…</div>
    </div>
    <div class="article ws">
        {{--        <h2>{{ __('messages.article_category') }} : {{ $article->category->category }}</h2>--}}
        {{--        <h2>{{ __('messages.article_level') }} : {{ $article->parity }}</h2><br>--}}
        <div class="article-content">
            <h1>{{ $article->title }}</h1><br>
            {!! $article->body !!}
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('body')
    <section class="articles">
        @foreach($articles as $article)
            @include('articles.includes.get_article')
        @endforeach
        @if($articles->total() > $articles->count())
            <br>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        {{ $articles->links() }}
                    </div>
                </div>
            </div>
        @endif
    </section>
@endsection
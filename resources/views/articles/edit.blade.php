@extends('layouts.app')

@section('body')
    @php
        /** @var \App\Models\Article $item */
    @endphp
    <section>
        @include('articles.includes.result_message')
        <form method="POST" action="{{ route('articles.store') }}">
            {{ csrf_field() }}
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('articles.includes.item_edit_add')
                </div>
            </div>
        </form>
    </section>
@endsection

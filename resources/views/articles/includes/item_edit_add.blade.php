@php
    /** @var App\Models\Article $item */
@endphp
Title
<input name="title" value="{{ $article->title }}"
       id="title"
       type="text"
       class="form-control"
       required>
<br>
Text
<br>
<textarea name="body"
          id="body"
          class="form-control"
          rows="20"
          required>{{ $article->body }}</textarea>
<br>
Category
<br>
<select name="category_id"
        id="category_id"
        class="form-control"
        placeholder="Вибираємо категорію"
        required>
    @foreach($categoryList as $categoryOption)
        <option value="{{$categoryOption->id}}">
            {{ $categoryOption->category_id }}
        </option>
    @endforeach
</select>
<br>
<br>
<button type="submit" class="btn btn-primary">Create</button>


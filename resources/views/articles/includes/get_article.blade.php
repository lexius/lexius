<a href="{{ route('articles.show', $article->id) }}" class="article-preview ws" style="background-image:url({{ $article->image }})">
    <div>
        <h3>{{ $article->title }}</h3>
        {{ $article->preview }}1
    </div>
    <span>{{ __('messages.article_preview_read') }}</span>
</a>
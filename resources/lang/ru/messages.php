<?php

return [

    'success_formula_step_1' => 'Читайте английские текста. Переводите новые слова.',
    'success_formula_step_2' => 'Ежедневно проходите по списку новых слов.',
    'success_formula_step_3' => 'Повторите.',

    'level_1' => 'Начинающий',
    'level_2' => 'Средний',
    'level_3' => 'Продвинутый',

    'menu_read_articles' => 'Читать',
    'menu_words_list' => 'Мои слова',
    'menu_add_article' => 'Добавить',
    'menu_login' => 'Войти',
    'menu_logout' => 'Выйти',

    'title_fresh_articles' => 'Свежие текста',
    'title_categories' => 'Категории',
    'title_articles_for_beginners' => 'Текста для начинающих',

    'article_preview_read' => 'Читать',
    'article_category' => 'Категория',
    'article_level' => 'Уровень',
    'article_search' => 'Поиск',
    'article_search_all_options' => 'Все',
    'article_keywords_in_title' => 'Содержит в названии',
    'article_keywords_in_title_placeholder' => 'Введите фразу на английском',
    'article_search_submit' => 'Найти',
    'article_translate_in_progress' => 'Переводим',
];
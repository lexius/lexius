<?php

return [

    'success_formula_step_1' => 'Read Spain articles. Translate new words.',
    'success_formula_step_2' => 'Everyday go through the list of your words.',
    'success_formula_step_3' => 'Repeat.',

    'level_1' => 'Beginner',
    'level_2' => 'Intermediate',
    'level_3' => 'Advanced',

    'menu_read_articles' => 'Read articles',
    'menu_words_list' => 'My words',
    'menu_add_article' => 'Add',
    'menu_login' => 'Login',
    'menu_logout' => 'Logout',

    'title_fresh_articles' => 'Fresh articles',
    'title_categories' => 'Categories',
    'title_articles_for_beginners' => 'Articles for the beginners',

    'article_preview_read' => 'Read',
    'article_category' => 'Category',
    'article_level' => 'Level',
    'article_search' => 'Browse articles',
    'article_search_all_options' => 'All',
    'article_keywords_in_title' => 'Title contains',
    'article_keywords_in_title_placeholder' => 'Input phrase in english',
    'article_search_submit' => 'Find articles',
    'article_translate_in_progress' => 'Translating',
];
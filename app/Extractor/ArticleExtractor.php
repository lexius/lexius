<?php


namespace App\Extractor;


class ArticleExtractor
{
    public function getContent($url)
    {
        $content = file_get_contents($url);

        if (!preg_match("/<article[^>]*>(.+)<\/article>/iUs", $content, $result)) {
            throw new \Exception('no article');
        }
        $content = $result[1];
        $content = preg_replace("/<script[^>]*>.+<\/script>/iUs", "", $content);
        $num = preg_match_all("/(<p(>|\s[^>]+>).+<\/p>|<h\d(>|\s[^>]+>).+<\/h\d>|<img[^>]+>)/iUs", $content, $result);

        if (!$num) {
            throw new \Exception('no ps found');
        }
        $elements = [];

        for ($i = 0; $i < $num; $i++) {
            preg_match("/^<(\w+)\W/iUs", $result[1][$i], $subresult);
            $method = 'processElement' . $subresult[1];
            $element = $this->$method($result[1][$i]);

            if ($element) {
                $elements[] = $element;
            }
        }

        if(count($elements) < 5){
            throw new \Exception('Too few elements');
        }

        return $content = implode('', $elements);
    }

    protected function processElementP($tag)
    {
        return sprintf('<p>%s</p>', $this->baseTextProcess($tag));
    }

    protected function processElementImg($tag)
    {
        if (!preg_match("/src=(\"(.+)\"|'(.+)')/iUs", $tag, $result)) {
            return null;
        }
        $src = $result[2];

        if (preg_match("/^data/", $src)) {
            $src = null;
        }

        if (!$src) {
            if (!preg_match("/data\-src=(\"(.+)\"|\'(.+)\')/iUs", $tag, $result)) {
                return null;
            }
            $src = $result[2];

            if (preg_match("/^data/", $src)) {
                return null;
            }
        }

        return sprintf('<img src="%s">', $result[2]);
    }

    protected function processElementH1($tag)
    {
        return $this->processElementH($tag);
    }

    protected function processElementH2($tag)
    {
        return $this->baseTextProcess($tag);
    }

    protected function processElementH3($tag)
    {
        return $this->baseTextProcess($tag);
    }

    protected function processElementH4($tag)
    {
        return $this->baseTextProcess($tag);
    }

    protected function processElementH5($tag)
    {
        return $this->baseTextProcess($tag);
    }

    protected function processElementH6($tag)
    {
        return $this->baseTextProcess($tag);
    }

    protected function processElementH($tag)
    {
        return sprintf('<h2>%s</h2>', $this->baseTextProcess($tag));
    }

    protected function baseTextProcess($string)
    {
        $string = trim(strip_tags($string, '<b><i><u><a><img>'));
        $string = preg_replace("/\s\s+/", " ", $string);

        return utf8_decode($string);
    }
}
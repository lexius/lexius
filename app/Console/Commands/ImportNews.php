<?php

namespace App\Console\Commands;

use App\Extractor\ArticleExtractor;
use App\Models\Article;
use App\Resolvers\TextSeverityResolver;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class ImportNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var ArticleExtractor
     */
    private $articleExtractor;
    /**
     * @var TextSeverityResolver
     */
    private $textSeverityResolver;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ArticleExtractor $articleExtractor, TextSeverityResolver $textSeverityResolver)
    {
        parent::__construct();
        $this->articleExtractor = $articleExtractor;
        $this->textSeverityResolver = $textSeverityResolver;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // https://newsapi.org/docs/endpoints/sources
        $map = '
            1 en 6 entertainment   buzzfeed    
            1 en 1 technology      techcrunch
            0 en 1 technology      techradar
            0 en 4 science         new-scientist    
            0 en 3 technology      the-verge    
            0 en 3 technology      engadget
            0 en 1 science         next-big-future
        ';
//        1 es 1 world           el-mundo
        $sources = [];

        foreach (explode(PHP_EOL, trim($map)) as $item) {
            $item = preg_replace("/\s+/", "\t", trim($item));
            $item = explode("\t", $item);

            if (!$item[0]) {
                continue;
            }
            $sources[$item[4]] = [
                'language' => $item[1],
                'category_id' => $item[2],
                'category' => $item[3],
                'name' => $item[4],
            ];
        }

        $baseUrl = 'https://newsapi.org/v2/top-headlines';
        $params = [
            'apiKey' => config('app.newsapi_key'),
            'sources' => 'el-mundo',//implode(',', array_keys($sources)),
            'language' => 'es',
        ];

        $url = sprintf('%s?%s', $baseUrl, http_build_query($params));
        $response = (new Client())->get($url);
        $result = json_decode($response->getBody()->getContents(), true);
        foreach ($result['articles'] as $article) {
            echo '•' . PHP_EOL;
//            echo $article['title'].PHP_EOL;
//            echo $article['url'].PHP_EOL;
//            continue;
            try {
                $content = $this->articleExtractor->getContent($article['url']);
            } catch (Exception $e) {
                echo $e->getMessage() . PHP_EOL;
                continue;
            }
            $existing = Article::where('title', $article['title'])->first();

            if ($existing instanceof Article) {
                continue;
            }

            echo $article['title'] . PHP_EOL;
            echo $article['url'] . PHP_EOL;
            try {
                Article::create([
                    'title' => $article['title'],
                    'preview' => $article['description'],
                    'body' => $content,
                    'parity' => $this->textSeverityResolver->resolve(strip_tags($content)),
                    'source' => 'newsapi.org/' . $article['source']['id'],
                    'image' => $article['urlToImage'] ?? '',
                    'url' => $article['url'],
                    'category_id' => $sources[$article['source']['id']]['category_id'],
                    'language' => $sources[$article['source']['id']]['language'],
                ]);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}

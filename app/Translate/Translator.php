<?php


namespace App\Translate;


use GuzzleHttp\Client;

class Translator
{
    public function translate($word)
    {
        if (config('app.fake_translate') == 1) {
            return $word;
        }
        $key = config('app.translate_yandex_api_key');
        $lang = config('app.language_translate');
        $apiRequest = "?key=$key&text=$word&lang=$lang";

        $client = new Client();
        $response = $client->post('https://translate.yandex.net/api/v1.5/tr.json/translate' . $apiRequest);

        $json = $response->getBody()->getContents();
        $result = json_decode($json, true);

        return $result['text']['0'];
    }
}
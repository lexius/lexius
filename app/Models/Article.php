<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'id',
        'title',
        'preview',
        'body',
        'parity',
        'source',
        'url',
        'image',
        'category_id',
        'language',
    ];

    public function category()
    {
        //Статя належить категорії
        return $this->belongsTo(Category::class);
    }

}
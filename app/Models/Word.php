<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $fillable = [
        'en',
        'ru',
    ];

    public function users ()
    {
        return $this->belongsToMany(User::class, 'users_words');
    }
}

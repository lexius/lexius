<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Repositories\ArticleRepository;
use App\Http\Requests\ArticleCreateRequest;
use App\Repositories\CategoryRepository;
use App\Resolvers\TextSeverityResolver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ArticleController extends Controller
{
    private $articleRepository;
    private $categoryRepository;
    private $textSeverityResolver;

    public function __construct(TextSeverityResolver $textSeverityResolver)
    {
        $this->articleRepository = app(ArticleRepository::class);
        $this->categoryRepository = app(CategoryRepository::class);
        $this->textSeverityResolver = $textSeverityResolver;
    }

    public function index(Request $request)
    {
        $articlesLanguage = App::getLocale() == 'en' ? 'es' : 'en';
        $articles = Article::where('language', $articlesLanguage);
        $category = $request->get('category');
        $level = $request->get('level');
        $keyword = $request->get('keyword');

        if ($category) {
            $articles = Article::whereHas("category", function ($q) use ($category) {
                $q->where('id', '=', $category);
            });
        }

        if ($level) {
            $articles = $articles->where('parity', $level);
        }

        if ($keyword) {
            $articles = $articles->where('title', 'like', '%'.$keyword.'%');
        }
        $articles = $articles->get();

        return view('articles.index', [
            'articles' => $articles,
            'search' => $request->all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article();
        $categoryList = $this->categoryRepository->getForComboBox();

        return view('articles.edit',
            compact('article', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleCreateRequest $request)
    {
        $data = $request->input();
        $data['title'] = strip_tags($data['title']);
        $data['body'] = strip_tags($data['body']);
        $data['preview'] = str_limit($data['body']);

        try {
            $data['parity'] = $this->textSeverityResolver->resolve($data['body']);
        } catch (\Exception $e) {
            return back()->withErrors(['msg' => $e->getMessage()])->withInput();
        }
        $article = (new Article())->create($data);

        if ($article) {
            return redirect()->route('articles.index')
                ->with(['success' => 'Saved successfully']);
        } else {
            return back()->withErrors(['msg' => 'Error saved'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);

        return view('articles.show', compact('article'));

    }

    public function showArticleWhistCategory($id)
    {
        $articles = Article::query()->select('*')
            ->where('category_id', $id)
            ->paginate(15);

        return view('articles.category.index', compact('articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Word;
use Illuminate\Support\Facades\Auth;

class WordController extends Controller
{
    public function wordsList()
    {
        if(!Auth::check()){
            return redirect(route('google.login'));
        }
        $user_id = Auth::id();
        $words = Word::query()
            ->select('words.en', 'words.ru')
            ->join('users_words', 'word_id', '=', 'words.id')
            ->join('users', 'users.id', '=', 'users_words.user_id')
            ->where('users_words.user_id', '=', $user_id)
            ->paginate(10);

        return view('words.index', compact('words'));
    }
}
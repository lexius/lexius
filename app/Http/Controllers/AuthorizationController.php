<?php

namespace App\Http\Controllers;

use App\User;
use Laravel\Socialite\Facades\Socialite;

class AuthorizationController extends Controller
{
    public function loginGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackGoogle()
    {
        $socialUser = Socialite::driver('google')->user();
        $user = User::where('email', $socialUser->getEmail())->first();

        if(!$user instanceof User){
            $user = User::create([
                'name' => $socialUser->getName(),
                'email' => $socialUser->getEmail(),
                'picture' => $socialUser->getAvatar(),
                'location' => $socialUser['locale'],
                'password' => md5(microtime()),
            ]);
        }
        auth()->login($user);

        return redirect('/')
            ->withCookie(cookie()->forever('locale', $user->location));
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->to('/');
    }
}
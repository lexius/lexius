<?php

namespace App\Http\Controllers;

use App\Models\Word;
use App\Translate\Translator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TranslateController extends Controller
{
    /**
     * @var Translator
     */
    private $translator;

    public function __construct(Translator $translator)
    {

        $this->translator = $translator;
    }

    public function translate(Request $request)
    {
        // filtration
        $inputWord = $request->get('word');
        $originalWord = mb_strtolower(preg_replace('/[^\w\s\’]+/', ' ', $inputWord));

        // find existing or translate & save
        $translatedWord = Word::where('en', $originalWord)->first();

        if (!$translatedWord instanceof Word) {
            $translatedWord = Word::create([
                'en' => $originalWord,
                'ru' => $this->translator->translate($originalWord),
            ]);
        }

        // connect word to user if not connected
        if (Auth::check()) {
            $userWord = Word::query()
                ->join('users_words', 'words.id', '=', 'users_words.word_id')
                ->join('users', 'users.id', '=', 'users_words.user_id')
                ->where([
                    ['users.id', Auth::id()],
                    ['words.en', $translatedWord['en']]
                ])
                ->first();

            if (!$userWord instanceof Word) {
                DB::table('users_words')
                    ->insert([
                        'user_id' => Auth::id(),
                        'word_id' => $translatedWord['id'],
                    ]);
            }
        }

        // return result
        $response = JsonResponse::create([
            'original' => $translatedWord['en'],
            'translated' => $translatedWord['ru'],
        ]);

        return $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}
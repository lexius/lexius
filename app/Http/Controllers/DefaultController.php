<?php

namespace App\Http\Controllers;

use App\Extractor\ArticleExtractor;
use App\Models\Article;
use App\Models\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class DefaultController extends Controller
{
    private $articleExtractor;

    public function __construct(ArticleExtractor $articleExtractor)
    {
        $this->articleExtractor = $articleExtractor;
    }

    public function welcome()
    {
        $articlesLanguage = App::getLocale() == 'en' ? 'es' : 'en';
        $articles = [
            'fresh' => Article::where('language', $articlesLanguage)
                ->orderBy('id', 'DESC')
                ->limit(12)
                ->get(),

            'beginners' => Article::where('language', $articlesLanguage)
                ->where('parity', '=', 1)
                ->limit(12)
                ->get(),
        ];

        return view('welcome', [
            'articles' => $articles,
            'categories' => Category::all(),
        ]);
    }

    public function locale($locale)
    {
        if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            $user->location = $locale;
            $user->save();
        }
        return redirect('/')
            ->withCookie(
                cookie()->forever('locale', $locale)
            );
    }

    public function test(Request $request)
    {
        if ($request->get('url')) {
            $content = $this->articleExtractor->getContent($request->get('url'));
        } else {
            $content = '';
        }

        return view('test', [
            'content' => $content,
        ]);
    }

}

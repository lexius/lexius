<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->cookie('locale')) {
            $locale = Crypt::decrypt($request->cookie('locale'));
        }else{
            $locale = 'en';
        }

        if (!in_array($locale, ['en', 'ru'])) {
            $locale = 'en';
        }
        App::setLocale($locale);

        return $next($request);
    }
}

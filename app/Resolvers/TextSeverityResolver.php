<?php

namespace App\Resolvers;

use Exception;

class TextSeverityResolver
{
    public function resolve($text)
    {
        /*
         * countWord - к-во слов в тексте
         * countSentence - к-во предложений в тексте
         * ASL — средняя длина предложения в словах
         */

        $words = preg_split('/\s+/', $text);
        $countWord = count($words);

        if (count(array_unique($words)) <= 10) {
            return 1;
        }
        $countSentence = preg_match_all('/[^\s](\.|\!|\?)(?!\w)/', $text, $match);

        if ($countSentence == 0) {
            throw new Exception('Should contain more then 1 sentence');
        }
        $asl = $countWord / $countSentence;

        /*
         * fragments - выбераемо все гласные буквы из текста
         * countSyllables - считаем к-во слогов в тексте
         * ASW — средняя длина слова в слогах
         */
        $fragments = preg_split("/[^aeiouy]+/", $text);
        $countSyllables = count($fragments);
        $asw = $countSyllables / $countWord;

        // fre - Индекс удобочитаемости Флеша
        $fre = 206.835 - 1.015 * $asl - 84.6 * $asw;

        if (65 <= $fre) {
            return 1;
        }

        if (30 <= $fre && $fre <= 65) {
            return 2;
        }

        if (0 <= $fre && $fre <= 30) {
            return 3;
        }

        throw new Exception('Failed to resolve severity');
    }
}
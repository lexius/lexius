<?php


namespace App\Repositories;

use App\Models\Category as Model;


class CategoryRepository extends CoreRepository
{

    protected function getModelClass()
    {
        return Model::class;
    }

    public function getForComboBox()
    {
        $columns = implode(',', [
            'id',
            'CONCAT (category) AS category_id',
        ]);

        $result = $this
            ->startConditions()
            ->selectRaw($columns)
            ->toBase()
            ->get();

        return $result;
    }

}
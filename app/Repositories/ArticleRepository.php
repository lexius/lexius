<?php


namespace App\Repositories;

use App\Models\Article as Model;


class ArticleRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }


    /**
     * @param null $perPage
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $result = $this
            ->startConditions()
            ->with('category:id,category')
            ->paginate($perPage);

        return $result;
    }

}
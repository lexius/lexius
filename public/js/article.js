$(document).ready(function () {

    var translatePopup = $('#article-translate');
    var translatePopupOriginal = $('#article-translate-original');
    var translatePopupTranslated = $('#article-translate-translated');
    var translatePopupSentence = $('#article-translate-sentence');
    var translatePopupPreloader = $('#article-translate-preloader');

    $('.article').on('mouseup', function (e) {
        s = window.getSelection();
        var range = s.getRangeAt(0);
        var node = s.anchorNode;
        while (range.toString().indexOf(' ') != 0) {
            range.setStart(node, (range.startOffset - 1));
        }
        range.setStart(node, range.startOffset + 1);
        do {
            range.setEnd(node, range.endOffset + 1);

        } while (range.toString().indexOf(' ') == -1 && range.toString().trim() != '' && range.endOffset < node.length);
        var str = range.toString().trim();


        translatePopup.show();
        translatePopupOriginal.text(str);
        translatePopupTranslated.text('');
        translatePopupSentence.hide();
        translatePopupPreloader.show();

        $.post('/translate', {word: str, sentence: 'Sample'}, function (result) {
            translatePopupOriginal.text(result.original);
            translatePopupTranslated.text(result.translated);
            translatePopupPreloader.hide();
            translatePopupSentence.show();
        });
    });

    translatePopup.on('click', function(){
        translatePopup.fadeOut();
    })

});
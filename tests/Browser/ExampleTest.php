<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class ExampleTest extends DuskTestCase
{
    use DatabaseTransactions;

    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $uniqueTitle = substr(md5(microtime()), 0, 6);
            $browser
                ->visit('/')
                ->clickLink('Добавить')
                ->type('title', $uniqueTitle)
                ->type('body', 'Lorem ipsum. Second sentence. Second sentence. Second sentence. Second sentence.')
                ->press('Create')
                ->assertSee($uniqueTitle)
                ->assertSee('Lorem ipsum');
        });
    }

    public function testStripTags()
    {
        $this->browse(function (Browser $browser) {
            $uniqueTitle = md5(microtime());
            $browser
                ->visit('/')
                ->clickLink('Добавить')
                ->type('title', $uniqueTitle)
                ->type('body', 'Hello this is <tag> <b>. Second sentence.')
                ->press('Create')
                ->assertSee($uniqueTitle)
                ->assertSee('Hello this is')
                ->assertDontSee('<tag>');
        });
    }
}

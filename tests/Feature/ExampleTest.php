<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    public function testHomePage()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testArticlesPage()
    {
        $response = $this->get('/articles');
        $response->assertStatus(200);
        $response->assertSeeText('Tesla');
    }

    public function testAddArticle()
    {
        $this->visit('/articles/create')
            ->type('SuperTitle', 'title')
            ->type('SuperBody', 'body')
            ->press('Create')
            ->seePageIs('/articles');
    }
}

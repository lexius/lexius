<?php

Route::get('/', 'DefaultController@welcome')->name('welcome');
Route::post('translate', 'TranslateController@translate');
Route::get('/articles/category/{category_id}' , 'ArticleController@showArticleWhistCategory')->name('article_category');
Route::get('/words', 'WordController@wordsList')->name('words.list');
Route::get('/locale/{locale}' , 'DefaultController@locale')->name('user.set_locale');
Route::get('/login/google', 'AuthorizationController@loginGoogle')->name('google.login');
Route::get('/callback/google', 'AuthorizationController@callbackGoogle');
Route::get('/logout', 'AuthorizationController@logout')->name('logout');
Route::any('/test', 'DefaultController@test')->name('test');
Route::post('/search', 'ArticleController@search')->name('articles.search');

Route::prefix('/')->group(function () {
    Route::resource('articles', 'ArticleController');
});